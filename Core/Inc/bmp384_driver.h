/*
 * bmp384.c
 *
 *  Created on: May 28, 2022
 *  Edited: July 20, 2022 by Vic
 *      Author: thodoris
 */



#ifndef INC_BMP384_DRIVER_H_
#define INC_BMP384_DRIVER_H_

#include "bmp384.h"
#include "stm32l4xx.h"

extern I2C_HandleTypeDef hi2c1;

HAL_StatusTypeDef bmp_read(uint16_t address, uint16_t MemAddSize, uint8_t *pData, uint16_t size);
HAL_StatusTypeDef bmp_write(uint16_t address, uint16_t MemAddSize, uint8_t *pData, uint16_t size);
void bmp_delay(uint32_t delay_ms);

#endif /* INC_BMP384_DRIVER_H_ */
