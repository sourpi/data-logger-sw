/*
 * LSM9DSO.c
 *
 *  Created on: Apr 13, 2022
 *      Author: Admin
 */
#include "LSM9DS0.h"
#include <string.h>
//#include "main.h"

_lsm9ds0_sensor sensor;
_lsm9ds0_sensor *imu = &sensor;

SPI_HandleTypeDef spi_sel;


//init values (fixed values) by datasheet
void init_LSM9DS0_struct(){
	imu->max_value_g_index = 0;
	imu->calib_arr_gyro[0] = 8.75;
	imu->calib_arr_gyro[1] = 17.50;
	imu->calib_arr_gyro[2] = 70;
	imu->gyr_base_sensitivity = imu->calib_arr_gyro[imu->max_value_g_index];
	imu->current_sens_gyr = imu->gyr_base_sensitivity;
	imu->gyr_percentage_temp_sens_change = 0.02;
	imu->current_offset_gyro = 0.0;

	imu->max_value_acc_index = 0;
	imu->calib_arr_acc[0] = 0.061;
	imu->calib_arr_acc[1] = 0.122;
	imu->calib_arr_acc[2] = 0.183;
	imu->calib_arr_acc[3] = 0.244;
	imu->calib_arr_acc[4] = 0.732;
	imu->acc_base_sensitivity = imu->calib_arr_acc[imu->max_value_acc_index];
	imu->current_sens_acc = imu->acc_base_sensitivity;
	imu->acc_percentage_temp_sens_change = 0.015;
	imu->current_offset_acc = 0.0;

	imu->max_value_mag_index = 0;
	imu->calib_arr_mag[0] = 0.08;
	imu->calib_arr_mag[1] = 0.16;
	imu->calib_arr_mag[2] = 0.32;
	imu->calib_arr_mag[3] = 0.48;
	imu->mag_base_sensitivity = imu->calib_arr_mag[imu->max_value_mag_index];
	imu->current_sens_mag = imu->mag_base_sensitivity;
	imu->mag_percentage_temp_sens_change = 0.03;
	imu->current_offset_mag = 0.0;

	imu->temp_mul_factor = 1;
	return;
}

//which mode of fifo we are using for each module
uint8_t fifo_stream_g = 0; //(0 -> bypass mode, 1 -> stream mode for gyroscope module)
uint8_t fifo_stream_xm = 0; //(0 -> bypass mode, 1 -> stream mode for magnetometer-accelerometer module)

//inverse of 2's complement
uint8_t i_twos_complement(uint8_t number){
	if (number == 0x00) return number;
	number = (uint8_t)(~number);
	return number - 1;
}

float linear_transformation(float base_1, float base_2, float base_1_new){
	float base_2_new = (base_2 * base_1_new)/base_1;
	return base_2_new;
}


//function to select SPI for modularity purpose
void select_spi(SPI_HandleTypeDef spi){
	spi_sel = spi;
	return;
}

//enable gyro module
void un_select_LSM9DS0_gyro(uint8_t select){
	HAL_GPIO_WritePin(CS_G_GPIO_Port, CS_G_Pin, select);
	return;
}

//enable magnetometer-accelerometer module
void un_select_LSM9DS0_acc_mag(uint8_t select){
	HAL_GPIO_WritePin(CS_XM_GPIO_Port, CS_XM_Pin, select);
	return;
}

//function that writes a single register from the specified module (module is specified from type argument)
void LSM9DSO_write_reg(bool type, uint8_t reg, uint8_t value, uint8_t times){

	//which module to select
	if (type == GYRO) un_select_LSM9DS0_gyro(SELECT);
	else un_select_LSM9DS0_acc_mag(SELECT);

	//write single register if times = NOT_MUL_RW (not sequential registers)
	uint8_t write_val;
	if (times == NOT_MUL_RW) times = 0x00;
	else times = 0x40;

	//write command sent to module
	write_val = reg | times;
	if(HAL_SPI_Transmit(&spi_sel, &write_val, 1, 100) != HAL_OK){
		Error_Handler();
	}

	//send data to be written
	if(HAL_SPI_Transmit(&spi_sel, &value, 1, 100) != HAL_OK){
		Error_Handler();
	}

	//unselect module
	if (type == GYRO) un_select_LSM9DS0_gyro(UNSELECT);
	else un_select_LSM9DS0_acc_mag(UNSELECT);

	return;
}


//function that reads a single register from the specified module (module is specified from type argument)
uint8_t LSM9DSO_read_reg(bool type, uint8_t reg, uint8_t times){

	//which module to select
	if (type == GYRO) un_select_LSM9DS0_gyro(SELECT);
	else un_select_LSM9DS0_acc_mag(SELECT);

	//read single register if times = NOT_MUL_RW (not sequential registers)
	uint8_t write_val;
	if (times == NOT_MUL_RW) times = 0x80;
	else times = 0xC0;

	//read command sent to module
	write_val = times | reg;
	if(HAL_SPI_Transmit(&spi_sel, &write_val, 1, 100) != HAL_OK){
		Error_Handler();
	}

	//module sends the data and they are stored to value variable
	uint8_t value;
	if(HAL_SPI_Receive(&spi_sel, &value, 1, 100) != HAL_OK){
		Error_Handler();
	}

	//unselect module
	if (type == GYRO) un_select_LSM9DS0_gyro(UNSELECT);
	else un_select_LSM9DS0_acc_mag(UNSELECT);

	return value;
}

//initialization of gyro module and struct
void LSM9DS0_init_gyro(){

	memset(imu->gyr_prev_raw, 0, 3*sizeof(uint16_t));
	memset(imu->gyr_prev_cal, 0, 3*sizeof(uint16_t));
	memset(imu->gyr_raw, 0, 3*sizeof(uint16_t));
	memset(imu->gyr_cal, 0, 3*sizeof(uint16_t));
	//memset(&imu->gyr_status, 0, 1*sizeof(uint16_t));

	//0x20 -> 0b01111111, ODR -> 190Hz, LPF -> 70Hz, normal mode, all axes enabled
	LSM9DSO_write_reg(GYRO, CTRL_REG1_G, 0x7F, NOT_MUL_RW);
	//0x21 -> 0b00100101 HF mode configuration = Normal mode, HPF cut off 0.45Hz
	LSM9DSO_write_reg(GYRO, CTRL_REG2_G, 0x25, NOT_MUL_RW);

	return;
}

//initilization of magnetometer-accelerometer module and struct
void LSM9DS0_init_acc_mag(){

	memset(imu->acc_prev_raw, 0, 3*sizeof(uint16_t));
	memset(imu->acc_prev_cal, 0, 3*sizeof(uint16_t));
	memset(imu->acc_raw, 0, 3*sizeof(uint16_t));
	memset(imu->acc_cal, 0, 3*sizeof(uint16_t));
	//memset(&imu->acc_status, 0, 1*sizeof(uint16_t));

	memset(imu->mag_prev_raw, 0, 3*sizeof(uint16_t));
	memset(imu->mag_prev_cal, 0, 3*sizeof(uint16_t));
	memset(imu->mag_raw, 0, 3*sizeof(uint16_t));
	memset(imu->mag_cal, 0, 3*sizeof(uint16_t));
//	memset(&imu->mag_status, 0, 1*sizeof(uint16_t));

	//ACCEL
	//0x20, 0x7F -> 0b01111111, AODR -> 200Hz,no update until MSB&LSB read, all axes enabled
	LSM9DSO_write_reg(ACCMAG, CTRL_REG1_XM, 0x7F, NOT_MUL_RW);

	//MAGN
	//0x24, 0xEC -> 0b01101100 , M_RES = 11 (High Resolution), M_ODR = 011 -> 25Hz
	LSM9DSO_write_reg(ACCMAG, CTRL_REG5_XM, 0xEC, NOT_MUL_RW);

	//mag continuous mode and normal mode for high pass filter
	LSM9DSO_write_reg(ACCMAG, CTRL_REG7_XM, 0x00, NOT_MUL_RW);

	//50Hz antialiasing filter for accelerometer (ABW == 11), the other fields the default value
	LSM9DSO_write_reg(ACCMAG, CTRL_REG2_XM, 0xC0, NOT_MUL_RW);

	return;
}

//function that reads gyro new data (if there aren't new data then waits for them)
void update_LSM9DSO_gyro(uint8_t check){

	uint8_t x_high_gyro;
	uint8_t x_low_gyro;

	uint8_t y_high_gyro;
	uint8_t y_low_gyro;

	uint8_t z_high_gyro;
	uint8_t z_low_gyro;

	//if bypass mode check for new data from STATUS_REG_G reg
	if (!fifo_stream_g && check){ //bypass fifo mode
		uint8_t status = 0;

		while(!status){
			status = ((LSM9DSO_read_reg(GYRO, STATUS_REG_G, NOT_MUL_RW) &0x0F) == 0x0F);
		}
	}

	//if stream mode the check for new data should be by
	//comparing the FSS4-FSS0 bits with 0 (FIFO_SRC_REG_G register)
	//before this function is called

	//read the data from the appropriate registers (for x,y,z directions separately)
	x_high_gyro = LSM9DSO_read_reg(GYRO, OUT_X_H_G, NOT_MUL_RW);
//	x_high_gyro = i_twos_complement(x_high_gyro);
	x_low_gyro = LSM9DSO_read_reg(GYRO, OUT_X_L_G, NOT_MUL_RW);
//	x_low_gyro = i_twos_complement(x_low_gyro);

	y_high_gyro = LSM9DSO_read_reg(GYRO, OUT_Y_H_G, NOT_MUL_RW);
//	y_high_gyro = i_twos_complement(y_high_gyro);
	y_low_gyro = LSM9DSO_read_reg(GYRO, OUT_Y_L_G, NOT_MUL_RW);
//	y_low_gyro = i_twos_complement(y_low_gyro);

	z_high_gyro = LSM9DSO_read_reg(GYRO, OUT_Z_H_G, NOT_MUL_RW);
//	z_high_gyro = i_twos_complement(z_high_gyro);
	z_low_gyro = LSM9DSO_read_reg(GYRO, OUT_Z_L_G, NOT_MUL_RW);
//	z_low_gyro = i_twos_complement(z_low_gyro);

	//store the raw (uncalibrated) previous data
	memcpy(imu->gyr_prev_raw, imu->gyr_raw, 3*sizeof(uint16_t));

	//store the new raw data
	imu->gyr_raw[0] = (x_high_gyro << 8) | x_low_gyro;
	imu->gyr_raw[1] = (y_high_gyro << 8) | y_low_gyro;
	imu->gyr_raw[2] = (z_high_gyro << 8) | z_low_gyro;
//	imu->gyr_cal =

	return;
}

//function that reads accelerometer new data (if there aren't new data then waits for them)
void update_LSM9DSO_acc(uint8_t check){

	uint8_t x_high_acc;
	uint8_t x_low_acc;

	uint8_t y_high_acc;
	uint8_t y_low_acc;

	uint8_t z_high_acc;
	uint8_t z_low_acc;

	//if bypass mode check for new data from STATUS_REG_A reg
	if (!fifo_stream_xm && check){ //bypass fifo mode
		uint8_t status = 0;

		while(!status){
			status = ((LSM9DSO_read_reg(ACCMAG, STATUS_REG_A, NOT_MUL_RW) & 0x0F) == 0x0F) ;
		}
	}

	//if stream mode the check for new data should be by
	//comparing the FSS4-FSS0 bits with 0 (FIFO_SRC_REG register)
	//before this function is called

	//read the data from the appropriate registers (for x,y,z directions separately)
	x_high_acc = LSM9DSO_read_reg(ACCMAG, OUT_X_H_A, NOT_MUL_RW);
//	x_high_acc = i_twos_complement(x_high_acc);
	x_low_acc = LSM9DSO_read_reg(ACCMAG, OUT_X_L_A, NOT_MUL_RW);
//	x_low_acc = i_twos_complement(x_low_acc);

	y_high_acc = LSM9DSO_read_reg(ACCMAG, OUT_Y_H_A, NOT_MUL_RW);
//	y_high_acc = i_twos_complement(y_high_acc);
	y_low_acc = LSM9DSO_read_reg(ACCMAG, OUT_Y_L_A, NOT_MUL_RW);
//	y_low_acc = i_twos_complement(y_low_acc);

	z_high_acc = LSM9DSO_read_reg(ACCMAG, OUT_Z_H_A, NOT_MUL_RW);
//	z_high_acc = i_twos_complement(z_high_acc);
	z_low_acc = LSM9DSO_read_reg(ACCMAG, OUT_Z_L_A, NOT_MUL_RW);
//	z_low_acc = i_twos_complement(z_low_acc);

	//store the raw (uncalibrated) previous data
	memcpy(imu->acc_prev_raw, imu->acc_raw, 3*sizeof(uint16_t));

	//store the new raw data
	imu->acc_raw[0] = (x_high_acc << 8) | x_low_acc;
	imu->acc_raw[1] = (y_high_acc << 8) | y_low_acc;
	imu->acc_raw[2] = (z_high_acc << 8) | z_low_acc;

	return;
}

//function that reads magnetometer new data (if there aren't new data then waits for them)
void update_LSM9DSO_mag(uint8_t check){

	uint8_t x_high_mag;
	uint8_t x_low_mag;

	uint8_t y_high_mag;
	uint8_t y_low_mag;

	uint8_t z_high_mag;
	uint8_t z_low_mag;

	//check for new data from STATUS_REG_M reg
	uint8_t status = 0;
	while(!status && check){
		status = ((LSM9DSO_read_reg(ACCMAG, STATUS_REG_M, NOT_MUL_RW) &0x0F) == 0x0F) ;
	}

	//read the data from the appropriate registers (for x,y,z directions separately)
	x_high_mag = LSM9DSO_read_reg(ACCMAG, OUT_X_H_M, NOT_MUL_RW);
//	x_high_mag = i_twos_complement(x_high_mag);
	x_low_mag = LSM9DSO_read_reg(ACCMAG, OUT_X_L_M, NOT_MUL_RW);
//	x_low_mag = i_twos_complement(x_low_mag);

	y_high_mag = LSM9DSO_read_reg(ACCMAG, OUT_Y_H_M, NOT_MUL_RW);
//	y_high_mag = i_twos_complement(y_high_mag);
	y_low_mag = LSM9DSO_read_reg(ACCMAG, OUT_Y_L_M, NOT_MUL_RW);
//	y_low_mag = i_twos_complement(y_low_mag);

	z_high_mag = LSM9DSO_read_reg(ACCMAG, OUT_Z_H_M, NOT_MUL_RW);
//	z_high_mag = i_twos_complement(z_high_mag);
	z_low_mag = LSM9DSO_read_reg(ACCMAG, OUT_Z_L_M, NOT_MUL_RW);
//	z_low_mag = i_twos_complement(z_low_mag);


	//store the raw (uncalibrated) previous data
	memcpy(imu->mag_prev_raw, imu->mag_raw, 3*sizeof(uint16_t));


	//store the new raw data
	imu->mag_raw[0] = (x_high_mag << 8) | x_low_mag;
	imu->mag_raw[1] = (y_high_mag << 8) | y_low_mag;
	imu->mag_raw[2] = (z_high_mag << 8) | z_low_mag;

	return;
}

//HPF enable = 0x10, fifo enable = 0x40 -> fifo & HPF enable = 0x50
void enable_fifo_and_HPF_g(){
	LSM9DSO_write_reg(GYRO, CTRL_REG5_G, 0x50, NOT_MUL_RW);
	return;
}

//fifo enable = 0x40
void enable_fifo_xm(){
	LSM9DSO_write_reg(ACCMAG, CTRL_REG0_XM, 0x40, NOT_MUL_RW);
	return;
}

//fifo stream mode mode = 0x40 for gyro
void set_FIFO_mode_gyro(uint8_t mode){
	//if mode == 0x40 -> stream mode -> fifo_stream_g = 1
	uint8_t prev_value = LSM9DSO_read_reg(GYRO, FIFO_CTRL_REG_G, NOT_MUL_RW);
	if (mode == 0x40) fifo_stream_g = 1;
	prev_value = prev_value & 0x1F;
	LSM9DSO_write_reg(GYRO, FIFO_CTRL_REG_G, mode | prev_value, NOT_MUL_RW);
	return;
}

//fifo stream mode mode = 0x40 for acc
void set_FIFO_mode_accmag(uint8_t mode){
	//if mode == 0x40 -> stream mode -> fifo_stream_xm = 1
	uint8_t prev_value = LSM9DSO_read_reg(ACCMAG, FIFO_CTRL_REG_G, NOT_MUL_RW);
	if (mode == 0x40) fifo_stream_xm = 1;
	prev_value = prev_value & 0x1F; //i want to override only the last 3bits
	LSM9DSO_write_reg(ACCMAG, FIFO_CTRL_REG, mode | prev_value, NOT_MUL_RW);
	return;
}


//function that sets the scale of the gyro
void LSM9DS0_set_gyro_scale(uint8_t scale){
	if(scale == SCALE_245DPS_G) imu->max_value_g_index = 0;
	if(scale == SCALE_500DPS_G) imu->max_value_g_index = 1;
	if(scale == SCALE_2000DPS_G) imu->max_value_g_index = 2;

	imu->gyr_base_sensitivity = imu->calib_arr_gyro[imu->max_value_g_index];
	uint8_t prev_value = LSM9DSO_read_reg(GYRO, CTRL_REG4_G, NOT_MUL_RW);
	prev_value = prev_value & 0xCF; //override only the first 2 bits of the second hex number
	LSM9DSO_write_reg(GYRO, CTRL_REG4_G, scale | prev_value, NOT_MUL_RW);
	return;
}

//function that sets the scale of the accelerometer
void LSM9DS0_set_acc_scale(uint8_t scale){
	if(scale == SCALE_2G_ACC) imu->max_value_acc_index = 0;
	if(scale == SCALE_4G_ACC) imu->max_value_acc_index = 1;
	if(scale == SCALE_6G_ACC) imu->max_value_acc_index = 2;
	if(scale == SCALE_8G_ACC) imu->max_value_acc_index = 3;
	if(scale == SCALE_16G_ACC) imu->max_value_acc_index = 4;

	imu->acc_base_sensitivity = imu->calib_arr_acc[imu->max_value_acc_index];
	uint8_t prev_value = LSM9DSO_read_reg(GYRO, CTRL_REG2_XM, NOT_MUL_RW);
	prev_value = prev_value & 0xC7; //override the first 2 bits of the second hex number and the last bit of first hex
	LSM9DSO_write_reg(GYRO, CTRL_REG2_XM, scale | prev_value, NOT_MUL_RW);
	return;
}

//function that sets the scale of the magnetometer
void LSM9DS0_set_mag_scale(uint8_t scale){
	if(scale == SCALE_2G_MAG) imu->max_value_mag_index = 0;
	if(scale == SCALE_4G_MAG) imu->max_value_mag_index = 1;
	if(scale == SCALE_8G_MAG) imu->max_value_mag_index = 2;
	if(scale == SCALE_16G_MAG) imu->max_value_mag_index = 3;

	imu->mag_base_sensitivity = imu->calib_arr_mag[imu->max_value_mag_index];
	uint8_t prev_value = LSM9DSO_read_reg(GYRO, CTRL_REG6_XM, NOT_MUL_RW);
	prev_value = prev_value & 0x00; //initialize scale to 00 (2gauss and override it with the scale argument)
	LSM9DSO_write_reg(GYRO, CTRL_REG6_XM, scale | prev_value, NOT_MUL_RW);
	return;
}

//mean value of new data (mean filter)
void calib_acc_data(){

	//update sensitivity based to temperature sensor
	LSM9DS0_update_acc_sensitivity();

	int32_t average_acc_x = 0;
	int32_t average_acc_y = 0;
	int32_t average_acc_z = 0;

	uint8_t samples = 1;
	uint8_t empty = 1;
	//if stream mode
	if(fifo_stream_xm){
		//loop until new data arrive
		do{
			uint8_t fifo_src_reg  = LSM9DSO_read_reg(ACCMAG, FIFO_SRC_REG, NOT_MUL_RW);
			samples = fifo_src_reg & 0x1F;
			empty = ((fifo_src_reg & 0x20) == 0x20);
		} while((samples == 0)|| empty);

	}

	//sum all the new data (in bypass mode the samples will remain 1)
	int i;
	for(i = 0; i < samples; i++){
		update_LSM9DSO_acc(fifo_stream_xm ^ 1);
		average_acc_x += imu->acc_raw[0]; //sign will remain after casting from 16 bit signed int to 32 bit signed int
		average_acc_y += imu->acc_raw[1];
		average_acc_z += imu->acc_raw[2];

	}

	//calculate the average of new data
	average_acc_x /= samples;
	average_acc_y /= samples;
	average_acc_z /= samples;
	int sign = 0;
	(average_acc_z > 0L) ? (sign = 1) : (sign = -1);
//	if(average_acc_z > 0L) {average_acc_z -= (int32_t) (1.0/imu->calib_arr_acc[imu->max_value_acc_index]);}  // Remove gravity from the z-axis accelerometer bias calculation
//	else {average_acc_z += (int32_t) (1.0/imu->calib_arr_acc[imu->max_value_acc_index]);}

	//copy the old data
	memcpy(imu->acc_prev_cal, imu->acc_cal, 3*sizeof(float));

	//calculate the new data based on the mean and the sensitivity
	imu->acc_cal[0] = (((float) average_acc_x) * imu->current_sens_acc)/1000;
	imu->acc_cal[1] = (((float) average_acc_y) * imu->current_sens_acc)/1000;
	//float temp = (((float) average_acc_z) * imu->current_sens_acc)/1000;
	imu->acc_cal[2] = (((float) average_acc_z) * imu->current_sens_acc)/1000;

	return;
}

void calib_mag_data(){

	//update sensitivity based to temperature sensor
	LSM9DS0_update_mag_sensitivity();

	//with argument = 1 to check for new data
	update_LSM9DSO_mag(1);

	//copy the old data
	memcpy(imu->mag_prev_cal, imu->mag_cal, 3*sizeof(float));

	//calculate the new data based on the mean and the sensitivity
	imu->mag_cal[0] = (((float) imu->mag_raw[0]) * imu->current_sens_mag)/1000;
	imu->mag_cal[1] = (((float) imu->mag_raw[1]) * imu->current_sens_mag)/1000;
	imu->mag_cal[2] = (((float) imu->mag_raw[2]) * imu->current_sens_mag)/1000;

	return;
}



void calib_gyro_data(){

	//update sensitivity based to temperature sensor
	LSM9DS0_update_gyr_sensitivity();

	int32_t average_gyr_x = 0;
	int32_t average_gyr_y = 0;
	int32_t average_gyr_z = 0;


	uint8_t samples = 1;
	uint8_t empty = 1;
	//if stream mode
	if(fifo_stream_xm){
		//loop until new data arrive
		do{
			uint8_t fifo_src_reg  = LSM9DSO_read_reg(GYRO, FIFO_SRC_REG_G, NOT_MUL_RW);
			samples = fifo_src_reg & 0x1F;
			empty = fifo_src_reg & 0x20;
		} while(samples == 0 || empty);

	}

	//sum all the new data (in bypass mode the samples will remain 1)
	int i;
	for(i = 0; i < samples; i++){
		update_LSM9DSO_gyro(fifo_stream_g ^ 1);
		average_gyr_x += imu->gyr_raw[0]; //sign will remain after casting from 16 bit signed int to 32 bit signed int
		average_gyr_y += imu->gyr_raw[1];
		average_gyr_z += imu->gyr_raw[2];

	}

	//calculate the average of new data
	average_gyr_x /= samples;
	average_gyr_y /= samples;
	average_gyr_z /= samples;

	//copy the old data
	memcpy(imu->mag_prev_cal, imu->mag_cal, 3*sizeof(float));

	//calculate the new data based on the mean and the sensitivity
	imu->gyr_cal[0] = (((float) average_gyr_x) * imu->current_sens_gyr)/1000;
	imu->gyr_cal[1] = (((float) average_gyr_y) * imu->current_sens_gyr)/1000;
	imu->gyr_cal[2] = (((float) average_gyr_z) * imu->current_sens_gyr)/1000;

	return;
}

//enable temperature
void LSM9DS0_enable_temperature(){
	uint8_t prev_value = LSM9DSO_read_reg(ACCMAG, CTRL_REG5_XM, NOT_MUL_RW);
	prev_value = prev_value | 0x80; //1 to MSB bit
	LSM9DSO_write_reg(ACCMAG, CTRL_REG5_XM, prev_value, NOT_MUL_RW);
	return;
}


//update temperature
void LSM9DSO_update_temp(){

	uint8_t x_high_temp;
	uint8_t x_low_temp;

	x_high_temp = LSM9DSO_read_reg(ACCMAG, OUT_TEMP_H_XM, NOT_MUL_RW);

	x_low_temp = LSM9DSO_read_reg(ACCMAG, OUT_TEMP_L_XM, NOT_MUL_RW);

	//save old raw temperature
	memcpy(&imu->temp_prev_raw, &imu->temp_raw, sizeof(int));

	imu->temp_raw =	(x_high_temp << 8) | x_low_temp;

	//save old calibrated temperature
	memcpy(&imu->temp_prev_cal, &imu->temp_cal, sizeof(float));

	//calculate enw temperature
	imu->temp_cal = ((float) imu->temp_raw)/8.0;
	return;
}


//calculate the new sensitivity starting from a base sensitivity
//and for each unit of temp_diff, sensitivity changes by change_sens%
float calc_new_sens(float base, float temp_diff, float change_sens){


	int sign;
	(temp_diff < 0) ? (sign = -1) : (sign = 1);

	float new_sens;
	float multiplier = pow((1 + sign*change_sens), (int) abs(temp_diff));
	new_sens = base * multiplier;

	return new_sens;
}

#define TEMP_OFFSET 15
//update the sensitivity of gyro
void LSM9DS0_update_gyr_sensitivity(){
	LSM9DSO_update_temp();
	float temp_diff = (imu->temp_cal + TEMP_OFFSET) - 25;
	imu->current_sens_gyr = calc_new_sens(imu->gyr_base_sensitivity, temp_diff, imu->gyr_percentage_temp_sens_change);

	return;
}

//update the sensitivity of accelerometer
void LSM9DS0_update_acc_sensitivity(){
	LSM9DSO_update_temp();
	float temp_diff = (imu->temp_cal + TEMP_OFFSET) - 25;
	imu->current_sens_acc = calc_new_sens(imu->acc_base_sensitivity, temp_diff, imu->acc_percentage_temp_sens_change);

	return;
}

//update the sensitivity of magnetometer
void LSM9DS0_update_mag_sensitivity(){
	LSM9DSO_update_temp();
	float temp_diff = (imu->temp_cal + TEMP_OFFSET) - 25;
	imu->current_sens_mag = calc_new_sens(imu->mag_base_sensitivity, temp_diff, imu->mag_percentage_temp_sens_change);

	return;
}

