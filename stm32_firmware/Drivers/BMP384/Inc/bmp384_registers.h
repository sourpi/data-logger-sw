/*
 * bmp384_registers.h
 *
 *  Created on: May 28, 2022
 *  Edited: July 20, 2022 by Vic
 *      Author: thodoris
 */


#ifndef BMP384_INC_BMP384_REGISTERS_H_
#define BMP384_INC_BMP384_REGISTERS_H_


/*--------------------- REGISTERS ADDRESSES --------------------- */

// Table 24: BPM384 Memory Map in Reversed order p.29


#define BMP384_REGISTER_CHIP_ID 		0x00 // (Bit 7..0) Chip id
#define BMP384_REGISTER_ERR_REG		 	0x02 // (Bit 0) fatal_err | (Bit 1) cmd_err | (Bit 2) conf_err
#define BMP384_REGISTER_STATUS			0x03 // (Bit 4) cmd_rdy() if 0: cmd in progress, if 1: ready to accept new command | (Bit 5) drdy_press -> Data ready for pressure| (Bit 6) drdy_temp -> Data ready for temperature sensor

//The 24Bit **Pressure data** is split and stored in three consecutive registers “DATA_0”,“DATA_1” and “DATA_2”
#define BMP384_REGISTER_DATA0 			0x04 // (Bit 7..0) PRESS_XLSB_7_0
#define BMP384_REGISTER_DATA1 			0x05 // (Bit 7..0) PRESS_LSB_15_8
#define BMP384_REGISTER_DATA2 			0x06 // (Bit 7..0) PRESS_MSB_23_16

//The 24Bit **Temperature data** is split and stored in three consecutive registers “DATA_3”,“DATA_4” and “DATA_5”
#define BMP384_REGISTER_DATA3 			0x07 //// (Bit 7..0) TEMP_XLSB_7_0
#define BMP384_REGISTER_DATA4 			0x08 //// (Bit 7..0) TEMP_LSB_15_8
#define BMP384_REGISTER_DATA5 			0x09 //// (Bit 7..0) TEMP_MSB_23_16

//The 24Bit **Sensor time** is split and stored in three consecutive registers “SENSORTIME_0”,“SENSORTIME_1” and “SENSORTIME_2”
#define BMP384_REGISTER_SENSORTIME0 	0x0c //// (Bit 7..0) sensor_time_7_0
#define BMP384_REGISTER_SENSORTIME1 	0x0d //// (Bit 7..0) sensor_time_15_8
#define BMP384_REGISTER_SENSORTIME2 	0x0e //// (Bit 7..0) sensor_time_23_16
#define BMP384_REGISTER_SENSORTIME3 	0x0f //// ?
/*In p.29 in the memory map there are 4 bytes allocated to sensor time data, however in p. 32 it says that the 24Bit
 sensor time is split and stored in three consecutive registers*/

#define BMP384_REGISTER_EVENT			0x10 // (0) por_detected -> contains the sensor status flags(‘1’ after device power up or softreset)

//The “INT_STATUS” register shows interrupt status and is cleared after reading.
#define BMP384_REGISTER_INT_STATUS		0x11
// (Bit 0) fwm_int -> FIFO Watermark Interrupt | (Bit 1) ffull_int -> FIFO Full Interrupt | (Bit 3) drdy -> Data ready for interrupt

//The FIFO byte counter indicates the current fill level of the FIFO buffer. Its size is 9 bit for 512 bytes and therefore split in two registers “FIFO_LENGTH_0” and “FIFO_LENGTH_1”
#define BMP384_REGISTER_FIFO_LENGTH_0 	0x12 //(Bit 7..0)
#define BMP384_REGISTER_FIFO_LENGTH_1   0x13 //(Bit 0)
#define BMP384_REGISTER_FIFO_DATA		0x14 //(Bit 7..0) FIFO read data

//The FIFO Watermark size is 9 Bit and therefore written to the FIFO_WTM_0 and FIFO_WTM_1 registers
#define BMP384_REGISTER_FIFO_WTM_0		0x15//(Bit 7..0)
#define BMP384_REGISTER_FIFO_WTM_1		0x16//(Bit 0)

#define BMP384_REGISTER_FIFO_CONFIG_1 	0x17
//(Bit 0) fifo_mode -> if 0: disable FIFO, if 1: enable FIFO mode
//(Bit 1) fifo_stop_on_full -> if 0: do not stop writing to FIFO when full , if 1: Stop writing into FIFO when full
//(Bit 2) fifo_time_en -> if 0: do not return sensortime frame, if 1: enable return sensortime frame after the last valid data frame
//(Bit 3) fifo_press_en -> if 0: no pressure data is stored , if 1:  pressure data is stored
//(Bit 4) fifo_temp_en -> if 0: no temperature data is stored, if 1: temperature data is stored

#define BMP384_REGISTER_FIFO_CONFIG_2 	0x18
//(Bit 2..0) fifo_subsampling -> FIFO downsampling selection for pressure and temperature data, factor is2^fifo_subsampling
//(Bit Bit 4..3) data_select -> if 0: unfiltered data, if 1: filtered data, if 11 or 10:reserved, same as for “unfilt”

//Interrupt configuration can be set in the “INT_CTRL” register. It affects INT_STATUS registers and the INT pin
#define BMP384_REGISTER_INT_CTRL 		0x19// (Bit 0..6) p.34

//The “IF_CONF” register controls the serial interface settings.
#define BMP384_REGISTER_IF_CONF 		0x1a
//(Bit 0) spi3 -> if 0: SPI 4-wire mode, if 1: SPI 3-wire mode
//(Bit 1) i2c_wdt_en -> if 0: I2C watchdog disabled, if 1: I2C watchdog enabled
//(Bit 2) i2c_wdt_sel -> if 0: I2C watchdog timeout after 1.25 ms, if 1: I2C watchdog timeout after 40 ms

/// A watchdog timer is a simple countdown timer which is used to reset a microcontroller after a specific interval of time to avoid any sw anomalies. After being restarted, the watchdog will begin timing another predetermined interval.

//The “PWR_CTRL” register enables or disables pressure and temperature measurement. Although, the measurement mode can be set here.
#define BMP384_REGISTER_PWR_CTRL		0x1b
//(Bit 0) press_en -> if 0: Disables the pressure sensor, if 1: Enables the pressure sensor
//(Bit 1) temp_en -> if 0: Disables the temperature sensor, if 1: Enables the temperature sensor
//(Bit 2) mode -> if 00: sleep mode, if 01/10: forced mode,if 11: normal mode

//The “OSR” register controls the oversampling settings for pressure and temperature measurements(p.36)
#define BMP384_REGISTER_OSR 			0x1c //(Bit 2..0) && (Bit 5..3)

// Set the Sampling Period(p.37)
#define BMP384_REGISTER_ODR 			0x1d //(Bit 4..0)

//IIR filter coefficients(p.38)
#define BMP384_REGISTER_CONFIG 	        0x1f ///(Bit 3..1)

//Available commands (see p.38)
#define BMP384_REGISTER_CMD 			0x7e ///(Bit 7..0)




/*--------------------- REGISTERS DATA --------------------- */




// Sensor Error conditions are reported in the “ERR_REG” register.(p.30)
#define BMP384_REGISTER_ERR_REG_FATAL_ERR_BIT 	0 // Fatal error
#define BMP384_REGISTER_ERR_REG_CMD_ERR_BIT 	1 // Command execution failed. Cleared on read.
#define BMP384_REGISTER_ERR_REG_CONF_ERR_BIT 	2 // sensor configuration error detected. Cleared on read.

// STATUS reg
// CMD decoder status.(p.30-31)
#define BMP384_REGISTER_STATUS_CMD_RDY_BIT		4 // 0: Command in progress && 1: Command decoder is ready to accept a new command
#define BMP384_REGISTER_STATUS_DRDY_PRESS_BIT	5 // Data ready for pressure. It gets reset, when one pressure DATA register is read out
#define BMP384_REGISTER_STATUS_DRDY_TEMP_BIT	6 // Data ready for temperature sensor.	It gets reset, when one temperature DATA register is read out

// DATA_0, DATA_1, DATA_2 => Pressure data
// DATA2 | DATA1 | DATA0

// DATA_3 , DATA_4, DATA_5 => Temprature data
// DATA5 | DATA4 | DATA3

// SENSOR 0,1,2 Sensor Time
// 2 | 1 | 0

// event
#define BMP384_REGISTER_EVENT_POR_DETECTED_BIT 		0 // ‘1’ after device power up or softreset. Clear-on-read

// int_status
#define BMP384_REGISTER_INT_STATUS_FWM_INT_BIT 		0 // FIFO Watermark Interrupt
#define BMP384_REGISTER_INT_STATUS_FFUL_INT_BIT 	1 // FIFO Full Interrupt
#define BMP384_REGISTER_INT_STATUS_DRDY_BIT 		3 // data ready interrupt

// FIFO LENGTH
// The FIFO byte counter indicates the current fill level of the FIFO buffer. Its size is 9 bit for 512 bytes and therefore split in two
// registers “FIFO_LENGTH_0” and “FIFO_LENGTH_1”
//  FIFO_LENGTH_1 | FIFLO_LENGTH_0

// FIFO_DATA
// Fifo read data
#define BMP384_REGISTER_FIFO_DATA_FIFO_DATA_MASK		0
#define BMP384_REGISTER_FIFO_DATA_FIFO_DATA_LENGTH		8

// FIFO WTM: 1bit wtm 1 | 8 bit wtm 0
// Fifo wtm 0
#define BMP384_REGISTER_FIFO_WTM_0_FIFO_WATER_MARK_7_0_MASK		0
#define BMP384_REGISTER_FIFO_WTM_0_FIFO_WATER_MARK_7_0_LENGTH	8

// Fifo wtm 1
#define BMP384_REGISTER_FIFO_WTM_1_FIFO_WATER_MARK_8_MASK 		0
#define BMP384_REGISTER_FIFO_WTM_1_FIFO_WATER_MARK_8_LENGTH		1

// Fifo config 1
#define BMP384_REGISTER_FIFO_CONFIG_1_FIFO_MODE_BIT 			0  // Enables or disables the FIFO: enabled(1)
#define BMP384_REGISTER_FIFO_CONFIG_1_FIFO_STOP_ON_FULL_BIT 	1  // Stop writing samples into FIFO when FIFO is full. enable(1)
#define BMP384_REGISTER_FIFO_CONFIG_1_FIFO_TIME_EN_BIT			2  // Return sensortime frame after the last valid data frame. enable(1)
#define BMP384_REGISTER_FIFO_CONFIG_1_FIFO_PRESS_EN_BIT			3  // Store pressure data in FIFO: enabled(1)
#define BMP384_REGISTER_FIFO_CONFIG_1_FIFO_TEMP_EN_BIT			4  // Store temperature data in FIFO


// Fifo config 2

// FIFO downsampling selection for pressure and temperature data, factor is 2^fifo_subsampling
#define BMP384_REGISTER_FIFO_CONFIG_2_FIFO_SUBSAMPLING_MASK		0
#define BMP384_REGISTER_FIFO_CONFIG_2_FIFO_SUBSAMPLING_LENGTH	3

/*
 *  for pressure and temperature, select data source
 *  0: unfiltered data (compensated or uncompensated)
 *  1: filtered data (compensated or uncompensated)
 *  11/10: reserved, same as for “unfilt”
 */
#define BMP384_REGISTER_FIFO_CONFIG_2_DATA_SELECT_MASK			3
#define BMP384_REGISTER_FIFO_CONFIG_2_DATA_SELECT_LENGTH		2

// Int control
#define BMP384_REGISTER_INT_CTRL_INT_OD_BIT		0 // Configure output: push-pull (0) or open-drain (1)
#define BMP384_REGISTER_INT_CTRL_INT_LEVEL_BIT	1 // level of INT pin: active_low(0) or active_high(1)
#define BMP384_REGISTER_INT_CTRL_INT_LATCH_BIT	2 // Latching of interrupts for INT pin and INT_STATUS register: enabled(1)
#define BMP384_REGISTER_INT_CTRL_FWTM_EN_BIT 	3 // enable FIFO watermark reached interrupt for INT pin and INT_STATUS: enabled(1)
#define BMP384_REGISTER_INT_CTRL_FFULL_EN_BIT 	4 // enable Fifo full interrupt for INT pin and INT_STATUS: enabled(1)
#define BMP384_REGISTER_INT_CTRL_DRDY_EN_BIT 	6 // enable temperature / pressure data ready interrupt for INT pin and INT_STATUS: enabled(1)

// IF CONTROL
// Configure SPI Interface Mode for primary interface
#define BMP384_REGISTER_IF_CONF_SPI3_BIT		0 // 0: SPI4 wire mode & 1: SPI3 wire mode

// Enable for the I2C Watchdog timer, backed by NVM
#define BMP384_REGISTER_IF_CONF_I2C_WDT_EN_BIT	1 // 0: Watchdog disabled & 1: Watchdog enabled

// Select timer period for I2C Watchdog , backed by NVM
#define BMP384_REGISTER_IF_CONF_I2C_WDT_SEL_BIT	2 // 0: wdt_short I2C watchdog timeout after 1.25 ms, 1: wdt_long I2C watchdog timeout after 40 ms

// Power Control
#define BMP384_REGISTER_PWR_CTRL_PRESS_EN_BIT	0 // 1: enables pressure sensor
#define BMP384_REGISTER_PWR_CTRL_TEMP_EN_BIT	1 // 1: enables temprature sensor
#define BMP384_REGISTER_PWR_CTRL_MODE_MASK 		4
#define BMP384_REGISTER_PWR_CTRL_MODE_LENGTH	1
#define BMP384_REGISTER_PWR_CTRL_MODE_SLEEP		(0 << BMP384_REGISTER_PWR_CTRL_MODE_MASK)
#define BMP384_REGISTER_PWR_CTRL_MODE_FORCED	(1 << BMP384_REGISTER_PWR_CTRL_MODE_MASK) // or 2 << 4
#define BMP384_REGISTER_PWR_CTRL_MODE_NORMAL	(3 << BMP384_REGISTER_PWR_CTRL_MODE_MASK)

// OSR register
// OSR_P
#define BMP384_REGISTER_OSR_OSR_P_MASK 			0
#define BMP384_REGISTER_OSR_OSR_P_LENGTH		3
#define BMP384_REGISTER_OSR_OSR_P_0				(0 << BMP384_REGISTER_OSR_OSR_P_MASK) // x1		x1 oversampling
#define BMP384_REGISTER_OSR_OSR_P_1				(1 << BMP384_REGISTER_OSR_OSR_P_MASK) // x2		x2 oversampling
#define BMP384_REGISTER_OSR_OSR_P_2				(2 << BMP384_REGISTER_OSR_OSR_P_MASK) // x4		x4 oversampling
#define BMP384_REGISTER_OSR_OSR_P_3				(3 << BMP384_REGISTER_OSR_OSR_P_MASK) // x8		x8 oversampling
#define BMP384_REGISTER_OSR_OSR_P_4				(4 << BMP384_REGISTER_OSR_OSR_P_MASK) // x16	x16 oversampling
#define BMP384_REGISTER_OSR_OSR_P_5				(5 << BMP384_REGISTER_OSR_OSR_P_MASK) // x32	x32 oversampling
// OSR_T
#define BMP384_REGISTER_OSR_OSR_T_MASK 			3
#define BMP384_REGISTER_OSR_OSR_T_LENGTH		3
#define BMP384_REGISTER_OSR_OSR_T_0				(0 << BMP384_REGISTER_OSR_OSR_T_MASK) // x1		x1 oversampling
#define BMP384_REGISTER_OSR_OSR_T_1				(1 << BMP384_REGISTER_OSR_OSR_T_MASK) // x2		x2 oversampling
#define BMP384_REGISTER_OSR_OSR_T_2				(2 << BMP384_REGISTER_OSR_OSR_T_MASK) // x4		x4 oversampling
#define BMP384_REGISTER_OSR_OSR_T_3				(3 << BMP384_REGISTER_OSR_OSR_T_MASK) // x8		x8 oversampling
#define BMP384_REGISTER_OSR_OSR_T_4				(4 << BMP384_REGISTER_OSR_OSR_T_MASK) // x16	x16 oversampling
#define BMP384_REGISTER_OSR_OSR_T_5				(5 << BMP384_REGISTER_OSR_OSR_T_MASK) // x32	x32 oversampling
// ODR register Table 42, 43
// subdivision factor for pressure and temperature measurements is 2^value. Allowed values are 0..17. Other values are saturated at 17.
#define BMP384_REGISTER_ODR_SEL_MASK		0
#define BMP384_REGISTER_ODR_SEL_LENGTH	 	5
#define BMP384_REGISTER_ODR_SEL_ODR_200	 	0x00 // Prescaler: 1, 		ODR 200Hz, 		Sampling time: 5 ms
#define BMP384_REGISTER_ODR_SEL_ODR_100	 	0x01 // Prescaler: 2,		ODR 100Hz,		Sampling time: 10 ms
#define BMP384_REGISTER_ODR_SEL_ODR_50	 	0x02 // Prescaler: 4, 		ODR 50Hz, 		Sampling time: 20 ms
#define BMP384_REGISTER_ODR_SEL_ODR_25	 	0x03 // Prescaler: 8, 		ODR 25Hz, 		Sampling time: 40 ms
#define BMP384_REGISTER_ODR_SEL_ODR_12p5 	0x04 // Prescaler: 16, 		ODR 25/2Hz, 	Sampling time: 80 ms
#define BMP384_REGISTER_ODR_SEL_ODR_6p25 	0x05 // Prescaler: 32, 		ODR 25/4Hz, 	Sampling time: 160 ms
#define BMP384_REGISTER_ODR_SEL_ODR_3p1	 	0x06 // Prescaler: 64, 		ODR 25/8Hz, 	Sampling time: 320 ms
#define BMP384_REGISTER_ODR_SEL_ODR_1p5	 	0x07 // Prescaler: 128, 	ODR 25/16Hz, 	Sampling time: 640 ms
#define BMP384_REGISTER_ODR_SEL_ODR_0p78 	0x08 // Prescaler: 256,	   	ODR 25/32Hz, 	Sampling time: 1.280 s
#define BMP384_REGISTER_ODR_SEL_ODR_0p39 	0x09 // Prescaler: 512,    	ODR 25/64Hz, 	Sampling time: 2.560 s
#define BMP384_REGISTER_ODR_SEL_ODR_0p2	 	0x0a // Prescaler: 1024,   	ODR 25/128Hz, 	Sampling time: 5.120 s
#define BMP384_REGISTER_ODR_SEL_ODR_0p1	 	0x0b // Prescaler: 2048,   	ODR 25/256Hz, 	Sampling time: 10.24 s
#define BMP384_REGISTER_ODR_SEL_ODR_0p05 	0x0c // Prescaler: 4096,   	ODR 25/512Hz, 	Sampling time: 20.48 s
#define BMP384_REGISTER_ODR_SEL_ODR_0p02 	0x0d // Prescaler: 8192,   	ODR 25/1024Hz, 	Sampling time: 40.96 s
#define BMP384_REGISTER_ODR_SEL_ODR_0p01 	0x0e // Prescaler: 16384,  	ODR 25/2048Hz, 	Sampling time: 81.92 s
#define BMP384_REGISTER_ODR_SEL_ODR_0p006 	0x0f // Prescaler: 32768,  	ODR 25/4096Hz, 	Sampling time: 163.84 s
#define BMP384_REGISTER_ODR_SEL_ODR_0p003 	0x10 // Prescaler: 65536,  	ODR 25/8192Hz, 	Sampling time: 327.68 s
#define BMP384_REGISTER_ODR_SEL_ODR_0p0015 	0x11 // Prescaler: 131072, 	ODR 25/16384Hz, Sampling time: 655.36 s

// Config register Table 44
#define BMP384_REGISTER_CONFIG_IIR_FILTER_MASK			1
#define BMP384_REGISTER_CONFIG_IIR_FILTER_LENGTH 		3
#define BMP384_REGISTER_CONFIG_IIR_FILTER_COEF_0 		0
#define BMP384_REGISTER_CONFIG_IIR_FILTER_COEF_1 		1
#define BMP384_REGISTER_CONFIG_IIR_FILTER_COEF_3 		2
#define BMP384_REGISTER_CONFIG_IIR_FILTER_COEF_7 		3
#define BMP384_REGISTER_CONFIG_IIR_FILTER_COEF_15 		4
#define BMP384_REGISTER_CONFIG_IIR_FILTER_COEF_31		5
#define BMP384_REGISTER_CONFIG_IIR_FILTER_COEF_63 		6
#define BMP384_REGISTER_CONFIG_IIR_FILTER_COEF_127 		7

// CMD REGISTER table 45
#define BMP384_REGISTER_CMD_CMD_MASK 		0
#define BMP384_REGISTER_CMD_CMD_LENGTH		8

// CMD Available commands: table 46
#define BMP384_REGISTER_CMD_NOP					0x00
#define BMP384_REGISTER_CMD_EXTMODE_EN_MIDDLE	0x34	// see extmode_en_last
#define BMP384_REGISTER_CMD_FIFO_FLUSH			0xb0 	// Clears all data in the FIFO, does not change FIFO_CONFIG registers
#define BMP384_REGISTER_CMD_SOFTRESET			0xb6    // triggers a reset, all user configuration settings are overwritten with their default state
#endif /* BMP384_INC_BMP384_REGISTERS_H_ */
