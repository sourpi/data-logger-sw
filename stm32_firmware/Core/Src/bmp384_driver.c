/*
 * bmp384.c
 *
 *  Created on: May 28, 2022
 *  Edited: July 20, 2022 by Vic
 *      Author: thodoris
 */
\
/*The 7-bit device address is 111011x. The 6 MSB bits are fixed. The last bit is changeable by SDO value and can be changed
//during operation. Connecting SDO to GND results in slave address 1110110 (0x76); connection it to V DDIO results in slave
//address 1110111 (0x77), which is the same as BMP180’s I²C address. The SDO pin cannot be left floating; if left floating, the
//I²C address will be undefined. */

#include "bmp384_driver.h"

uint8_t DevAddress_read = 0b11101111;
uint8_t DevAddress_write = 0b11101110;

HAL_StatusTypeDef bmp_read(uint16_t address, uint16_t MemAddSize, uint8_t *pData, uint16_t size) {
	return HAL_I2C_Mem_Read(&hi2c1, DevAddress_read, address, MemAddSize, pData, size, HAL_MAX_DELAY);
}
HAL_StatusTypeDef bmp_write(uint16_t address, uint16_t MemAddSize, uint8_t *pData, uint16_t size) {
	return HAL_I2C_Mem_Write(&hi2c1, DevAddress_write, address, MemAddSize, pData, size, HAL_MAX_DELAY);
}

void bmp_delay(uint32_t delay_ms) {
	HAL_Delay(delay_ms);
}
