# Data Logger
- Data Logger utilizes the STM32L433RTx MCU and is equipped with the BMP384 (barometric data), LSM9DS0 (9 DoF IMU), 2 x MS5607 (barometric data) sensors in order to determine the rocket's kinematic characteristics and holds critical information regarding the parachute's deployment. 
- It has an SD card to retrieve the data after the flight.
- It is connected via CAN bus with the rest network of controllers (RMC & Comms).
- It also has a GPS module to assist in finding the rocket's location. 
